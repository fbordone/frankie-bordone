<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

// ==============================
// Composer autoloader if present
// ==============================
if ( file_exists( __DIR__ . '/wp-content/vendor/autoload.php' ) ) {
    define( 'USE_COMPOSER_AUTOLOADER', true );
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( __DIR__ . '/local-config.php' ) ) {
    define( 'WP_LOCAL_DEV', true );
    include( __DIR__ . '/local-config.php' );
} else {
    define( 'WP_LOCAL_DEV', false );
}
if ( ! defined( 'DB_NAME' ) ) {
    define( 'DB_NAME', 'production_db_name' );
}
if ( ! defined( 'DB_USER' ) ) {
    define( 'DB_USER', 'production_db_user' );
}
if ( ! defined( 'DB_PASSWORD' ) ) {
    define( 'DB_PASSWORD', 'production_db_password' );
}
if ( ! defined( 'DB_HOST' ) ) {
    define( 'DB_HOST', 'dbmaster' ); // 'dbmaster' for situation servers, 'localhost' elsewhere
}

// ========================
// File Editing and Updates
// Disable since core and plugin updates are managed through composer
// ========================
define( 'AUTOMATIC_UPDATER_DISABLED', true );
if ( ! defined( 'DISALLOW_FILE_MODS' ) ) {
    define( 'DISALLOW_FILE_MODS', true );
}

// Uncomment and create a dedicated server cron job
// define( 'DISABLE_WP_CRON', true );

// ========================
// Custom Content Directory
// Can be beneficial to define this in a local-config.php to force canonical urls,
// otherwise both www and non-www work by default
// ========================
if ( ! defined( 'WP_SITEURL' ) ) {
    define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp' );
}
if ( ! defined( 'WP_HOME' ) ) {
    define( 'WP_HOME', 'http://' . $_SERVER['SERVER_NAME'] . '/' );
}
if ( ! defined( 'WP_CONTENT_DIR' ) ) {
    define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
}
if ( ! defined( 'WP_CONTENT_URL' ) ) {
    define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'g0~Qe=J}0 Nali_,79jvv(CUuYIK+Ecl @qnPBM,bDE&t?:tAB,tLWZ#0<;iiDb7');
define('SECURE_AUTH_KEY',  '|L2p$-t,PKq4A,{.<zx&M$&ba`|Dqj!Cb&zij{2EeubiJ{+41#S)z6|JyWS4$(id');
define('LOGGED_IN_KEY',    'cdKN7PZ 8 -8`#Zdl+{:+J;c-ZoRWq Ict?+2.<,}*iph{atL^e0qIz=F504>!<@');
define('NONCE_KEY',        'jd69citBruEY{5I==gCTet1!r<Lrx,:g}Z|iZ6@OwD{`)tKd-a&iXVYI9oP6QH4k');
define('AUTH_SALT',        'tHA#` >&Pm1W<EGyR#Bmdi0XR}2K!8L@D0EsxtZGiNZP_NDx0|W;+0u8kG4ghXGL');
define('SECURE_AUTH_SALT', 'R.iC+!LyrR?F$+>5g@y!P9e:0q?W$3=6;w,b/gLOR-O)7.MN@A2Pxz=sM-9yb$~]');
define('LOGGED_IN_SALT',   '||paN0G~!>e(ue71;3J3LqWwwNn1+h~*lr19l,2@8TLu)<Gz_be4sZp<ER.pB-r]');
define('NONCE_SALT',       'QO4t[}|}eV[rJr}rb{+G %OO^`|t^/y.V *M*M1G.UCJ=Yb)nC)xh,30IP8A]t0j');



// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'wp_';

// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

// ===========
// Hide errors
// ===========
if ( ! defined( 'WP_DEBUG' ) ) {
    ini_set( 'display_errors', 0 );
    define( 'WP_DEBUG_DISPLAY', false );
}

// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );

// ======================================
// Load a Memcached config if we have one
// ======================================
if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) ) {
    $memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );
}

// ===================
// Bootstrap WordPress
// ===================
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
}
require_once( ABSPATH . 'wp-settings.php' );
