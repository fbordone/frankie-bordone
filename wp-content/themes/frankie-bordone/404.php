<div class="container-fluid">
    <?php get_template_part('templates/components/page-header'); ?>

    <div class="alert alert-warning">
        <?php _e('Sorry, but the page you were trying to view does not exist.', 'situation'); ?>
    </div>

</div>
