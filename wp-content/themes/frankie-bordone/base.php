<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--[if IE]>
<div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'situation'); ?>
</div>
<![endif]-->
<?php
if (apply_filters('show_site_header', true)){
    do_action('get_header');
    fb_template_part('templates/modules/header');
}
?>
<div class="wrap" role="document">
    <main class="main">
        <?php fb_template_path(); ?>
    </main><!-- /.main -->
</div><!-- /.wrap -->
<?php
if (apply_filters('show_site_footer', true)){
    do_action('get_footer');
    fb_template_part('templates/modules/footer');
}
wp_footer();
?>
</body>
</html>
