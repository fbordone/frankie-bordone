<?php
/**
 * Template Name: Home
 */

namespace Theme;
use Underground\Util;
use \WP_Image;
use Theme\Views\ServiceCardView;

global $post;
$Home = new Home($post->ID);
$GlobalOptions = new GlobalOptions();
$image = WP_Image::get_by_attachment_id($Home->about_image_id);

?>

<section class="hero" id="home">
	<div class="row">
		<h1><?= $Home->hero_header; ?></h1>
		<p class="hero__sub-header"><?= $Home->hero_sub_header; ?></p>
	</div>
</section>

<section class="about" id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<?= $image->width(Home::ABOUT_IMAGE_WIDTH)->custom_attr('class', 'about__image'); ?>
			</div>
			<div class="col-md-7">
				<div class="about__header-container">
					<h4 class="about__header about__header--active"><?= $Home->about_header; ?></h4>
					<h4 class="about__header"><?= $Home->skills_header; ?></h4>
				</div>

				<div class="about__description">
					<?= $Home->about_description; ?>

					<?php if ($Home->toggle_social) : ?>
						<?= Util::getTemplateScoped('templates/components/social-icons', ['social_icons' => $GlobalOptions->getSocialIcons()]); ?>
					<?php endif; ?>
				</div>

				<?= Util::getTemplateScoped('templates/components/skills', ['skills' => $Home->skills]); ?>
			</div>
		</div>
	</div>
</section>

<section id="services">
	<div class="container">
		<div class="row">
			<h2><?= $Home->services_header; ?></h2>
		</div>

		<div class="row">
			<?php foreach ($Home->service_cards as $card) : ?>
				<div class="col-md-4">
					<?= new ServiceCardView($card); ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<section id="photos">
	<div class="container">
		<div class="row">
			<h2>My Photos</h2>
		</div>

		<div class="row">
			<div id="instagram-feed"></div>
		</div>
	</div>
</section>

<section id="contact">
	<div class="container">
		<div class="row">
			<h2>Contact Me</h2>
		</div>

		<div class="row">
			<div class="col-md-7">
				<div class="contact-form__messaging">
					<div class="contact-form__success">
						<p>Your email has been successfully sent.</p>
					</div>
					<div class="contact-form__danger">
						<p>Please try again later.</p>
					</div>
				</div>
				<form class="contact-form form" role="form">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="screen-reader-text" for="name">Enter your name</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></div>
									<input class="form-control" name="name" type="text" placeholder="Name*" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="screen-reader-text" for="email">Enter your email</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
									<input class="form-control" name="email" type="email" placeholder="Email*" required>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="screen-reader-text" for="message">Enter a message</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-comment-o" aria-hidden="true"></i></div>
									<textarea class="form-control" name="message" placeholder="Message*" required></textarea>
								</div>
							</div>
						</div>
					</div>

					<button class="contact-form__submit">Submit</button>
				</form>
			</div>

			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12">
						<div class="contact-card">
							<div class="contact-card__icon">
								<i class="fa fa-map-marker fa-inverse" aria-hidden="true"></i>
							</div>

							<div class="contact-card__info">
								<h5 class="contact-card__header">My Location:</h5>
								<p class="text-marginless">2468 West Street, Brooklyn NY</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="contact-card">
							<div class="contact-card__icon">
								<i class="fa fa-envelope fa-inverse" aria-hidden="true"></i>
							</div>

							<div class="contact-card__info">
								<h5 class="contact-card__header">Email Address:</h5>
								<p class="text-marginless">bordone.francesco@gmail.com</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
