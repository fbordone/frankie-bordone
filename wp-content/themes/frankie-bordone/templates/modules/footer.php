<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<p class="footer__description">&copy;<?php echo date("Y"); ?> <span class="text-bold"><?php bloginfo('name'); ?>.</span> All Rights Reserved</p>
			</div>
		</div>
	</div>
</footer>
