<header class="header-nav">
	<div class="container">
		<div class="navbar-header">
			<button class="btn-hamburger" type="button" data-toggle="collapse" data-target="#bs-navigation" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a class="header-nav__brand" href="<?= home_url('/'); ?>"><?php bloginfo('name'); ?></a>
		</div>

		<nav class="collapse navbar-collapse" id="bs-navigation">
			<?php
				if (has_nav_menu('primary_navigation')) {
					wp_nav_menu(['theme_location' => 'primary_navigation']);
				}
			?>
		</nav>
	</div>
</header>
