<article <?php post_class(); ?>>
    <div class="entry-content">
        <?php the_content(); ?>
    </div>
    <footer>
        <?php wp_link_pages([
            'before' => '<nav class="page-nav"><p>' . __('Pages:', 'situation'),
            'after'  => '</p></nav>'
        ]); ?>
    </footer>
    <?php comments_template('/templates/components/comments.php'); ?>
</article>
