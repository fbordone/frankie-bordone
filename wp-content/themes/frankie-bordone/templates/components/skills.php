<?php
/**
 * Expected:
 * @var array $skills
 */

?>

<div class="about__skills display-none">
	<?php foreach ($skills as $skill) : ?>
		<h5><?= $skill['label']; ?></h5>
		<div class="about__progress-container">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?= $skill['percentage']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $skill['percentage']; ?>%"><?= $skill['percentage']; ?>%</div>
		</div>
	<?php endforeach; ?>
</div>
