<?php
/**
 * Expected:
 * @var string $icon
 * @var string $header
 * @var string $description
 * @var array $content_blocks
 */
?>
<div class="service-card">
	<i class="service-card__icon <?= $icon; ?>" aria-hidden="true"></i>
	<h4 class="service-card__header"><?= $header; ?></h4>
	<p class="service-card__description"><?= $description; ?></p>

	<?php foreach ($content_blocks as $block) : ?>
		<h5 class="service-card__list-header"><?= $block['header']; ?></h5>
		<?= $block['content']; ?>
	<?php endforeach; ?>
</div>
