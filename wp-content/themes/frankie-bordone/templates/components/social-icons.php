<?php
/**
 * Expected:
 * @var array $social_icons
 */

?>

<div class="social-icons">
	<?php foreach ($social_icons as $icon) : ?>
		<a href="<?= $icon['url']; ?>" class="social-icons__icon" title="<?= $icon['title']; ?>" target="_blank"><i class="fa <?= $icon['slug'];  ?>" aria-hidden="true"></i></a>
	<?php endforeach; ?>
</div>
