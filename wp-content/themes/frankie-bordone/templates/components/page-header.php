<header>
    <h1 class="entry-title"><?php //the_title(); ?></h1>
    <?php if (is_single()) {
        get_template_part('templates/components/entry-meta');
    } ?>
</header>
