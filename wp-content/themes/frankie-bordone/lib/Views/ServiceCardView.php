<?php

namespace Theme\Views;
use Underground\View\TemplateView;

/**
 * class ServiceCardView
 * @package Theme
 * @author Frankie Bordone <bordone.francesco@gmail.com>
 * @version 1.0
 * @property string $icon
 * @property string $header
 * @property string $description
 * @property array $content_blocks
 */
class ServiceCardView extends TemplateView {
	protected $name = 'components/service-card';
}

?>
