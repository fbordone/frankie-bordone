<?php

namespace Theme;
use Underground\Models\PostBase;

/**
 * class Home
 * @package Theme
 * @author Frankie Bordone <bordone.francesco@gmail.com>
 * @version 1.0
 * @property string $hero_header
 * @property string $hero_sub_header
 * @property string $about_image_id
 * @property string $about_header
 * @property string $about_description
 * @property string $skills_header
 * @property array $skills
 * @property string $services_header
 * @property array $service_cards
 */
class Home extends PostBase {
	const POST_TYPE = 'page';
	const ABOUT_IMAGE_WIDTH = 450;
	protected $hero_header;
	protected $hero_sub_header;
	protected $about_image_id;
	protected $about_header;
	protected $about_description;
	protected $skills_header;
	protected $skills;
	protected $services_header;
	protected $service_cards;
}

?>
