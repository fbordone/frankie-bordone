<?php

namespace Theme\Contact;

/**
 * Class Client
 * @package Theme\Contact
 * @author Frankie Bordone <bordone.francesco@gmail.com>
 * @version 1.0
 */
class Client {
	protected $client;

	function request($params)
	{
		$params = array_merge($this->params, $params);
		$url = add_query_arg($params, $this->endpoint_base);
		$response = wp_remote_post($url);
		return wp_remote_retrieve_body($response);
	}

	function sendEmail($params)
	{
		$response = $this->request($params);
		wp_mail(
			"bordone.francesco@gmail.com",
			"Frankiebordone.com: Contact Request",
			$params['message'] . $params['email'],
			$headers = [],
			$attachments = []
		);
		return $this->convertXmlToJson($response);
	}

	// Helpers

	protected function convertXmlToJson($response_body)
	{
		$xml = simplexml_load_string($response_body);
		$json = json_encode($xml, JSON_PRETTY_PRINT);
		return json_decode($json, true);
	}
}
