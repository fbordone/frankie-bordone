<?php

namespace Theme\Contact;
use Underground\Controller\RestController;

/**
 * Class ContactController
 * @package Theme\Contact
 * @author Frankie Bordone <bordone.francesco@gmail.com>
 * @version 1.0
 */
class ContactController extends RestController {
	protected $namespace = 'contact';
	protected $route_path = 'get';

	public function registerRoutes() {
		$this->addRoutes($this->route_path, [
			$this->addReadAction('execute')
		]);
	}

	public function execute(\WP_REST_Request $request) {
		$client = new Client();
		$response = $client->sendEmail();
		return rest_ensure_response($response);
	}
}

 ?>
