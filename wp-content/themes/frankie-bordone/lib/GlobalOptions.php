<?php

namespace Theme;

/**
 * class GlobalOptions
 * @package Theme
 * @author Frankie Bordone <bordone.francesco@gmail.com>
 * @version 1.0
 */
class GlobalOptions {
	public function getSocialIcons() {
		return get_field('social_icons', 'option');
	}
}

?>
