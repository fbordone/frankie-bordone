<?php

namespace Theme;

use Underground\Config;
use Underground\Html\LoadStack;
use Underground\Util\TemplateWrapper;
use Underground\Vendor\GravityForms;
use Theme\Contact\ContactController;

/**
 * Class Theme
 * @package Situation
 * @author  ccollier
 */
class Theme
{
	public function __construct()
	{
		new Config(['config_files' => [
			FB_CONFIG_DIR . '/global.json'
		]]);
		new LoadStack();
		new GravityForms();
		new ContactController();
		TemplateWrapper::init();
		add_action('after_setup_theme', [$this, 'setup']);
		add_action('wp_enqueue_scripts', [$this, 'assets'], 100);
		add_action('admin_enqueue_scripts', [$this, 'adminAssets'], 100);
		add_action('admin_init', [$this, 'removeEditorFromPages']);
		add_filter('body_class', [$this, 'bodyClass']);
		add_filter('excerpt_more', [$this, 'excerptMore']);
		//add_action('admin_menu', [$this, 'removeDefaultPostType']);
		add_filter('wpseo_metabox_prio', '__return_false');
		add_filter('gform_init_scripts_footer', '__return_true');
		add_filter('rest_url_prefix', function(){
			return 'api';
		});
	}

	/**
	 * Theme setup
	 */
	function setup()
	{
		// Enable features from Soil when plugin is activated
		// https://roots.io/plugins/soil/
		add_theme_support('soil-clean-up');
		add_theme_support('soil-disable-asset-versioning');
		add_theme_support('soil-disable-trackbacks');
		add_theme_support('soil-js-to-footer');
		add_theme_support('soil-nav-walker');
		add_theme_support('soil-nice-search');
		add_theme_support('soil-jquery-cdn');
		add_theme_support('soil-relative-urls');
		remove_theme_support('wp_video_api_ajax');

		// Enable plugins to manage the document title
		// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
		add_theme_support('title-tag');

		// Register wp_nav_menu() menus
		// http://codex.wordpress.org/Function_Reference/register_nav_menus
		register_nav_menus([
			'primary_navigation' => __('Primary Navigation', 'frankiebordone'),
		]);

		// Enable post thumbnails
		// http://codex.wordpress.org/Post_Thumbnails
		// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
		// http://codex.wordpress.org/Function_Reference/add_image_size
		add_theme_support('post-thumbnails');

		// Enable post formats
		// http://codex.wordpress.org/Post_Formats
		add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

		// Enable HTML5 markup support
		// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
		add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

		// Use main stylesheet for visual editor
		// To add custom styles edit /assets/styles/layouts/_tinymce.scss
		add_editor_style(fb_asset_path('styles/admin-editor.css'));
		add_filter('show_admin_bar', '__return_false');
	}

	/**
	 * Theme assets
	 */
	function assets()
	{
		wp_enqueue_style('theme/css', fb_asset_path('styles/main.css'), false, null);

		if (is_single() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

		wp_enqueue_script('theme/js', fb_asset_path('scripts/main.js'), ['jquery'], null, true);
		$js_vars = apply_filters('global_js_vars', ['ajax_url' => admin_url('admin-ajax.php')]);
		wp_localize_script('theme/js', 'sit', $js_vars);

		wp_enqueue_style('theme/fonts', 'https://fonts.googleapis.com/css?family=Montserrat:600,900|Poppins:400,700', false);
	}

	function adminAssets()
	{
		wp_enqueue_style('theme/admin/css', fb_asset_path('styles/admin-custom.css'), false, null);
		wp_enqueue_script('theme/admin/js', fb_asset_path('scripts/admin.js'));
	}

	/**
	 * Add <body> classes
	 */
	function bodyClass($classes)
	{
		// Add page slug if it doesn't exist
		if (is_single() || is_page() && ! is_front_page()) {
			if (! in_array(basename(get_permalink()), $classes)) {
				$classes[] = basename(get_permalink());
			}
		}

		return $classes;
	}

	/**
	 * Clean up the_excerpt()
	 */
	function excerptMore()
	{
		return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'situation') . '</a>';
	}

	function removeDefaultPostType()
	{
		remove_menu_page('edit.php');
		remove_menu_page('edit-comments.php');
	}

	function removeEditorFromPages()
	{
		remove_post_type_support('page', 'editor');
	}
}
