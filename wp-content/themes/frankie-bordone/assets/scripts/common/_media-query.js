var mediaQuery = (function() {
    var size = {
        lg: 1200,
        md: 992,
        sm: 768,
        xs: 480
    };
    var maxSize = {
        mdMax: size.lg - 1,
        smMax: size.md - 1,
        xsMax: size.sm - 1
    };

    var which = function() {
        var range = mediaQuery.range();
        return range.length ? range[0] : 'min';
    };
    var range = function() {
        var ret = [];
        _.forEach(size, function(value, key) {
            if (Modernizr.mq('(min-width: ' + value + 'px)')) {
                ret.push(key);
            }
        });
        return ret;
    };

    var isDesktop = function() {
        return Modernizr.mq('(min-width: ' + size.sm + 'px)');
    };
    var isMobile = function() {
        return !mediaQuery.isDesktop();
    };
    var viewport = {
        width: function() {
            return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        },
        height: function() {
            return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        }
    };
    var isLandscape = function() {
        return (viewport.width() / viewport.height() > 0.92 && viewport.width() > 480);
    };
    return {
        which: which,
        isDesktop: isDesktop,
        isMobile: isMobile,
        isLandscape: isLandscape,
        sizes: $.extend({}, size, maxSize),
        range: range,
        viewport: viewport
    };
})();
