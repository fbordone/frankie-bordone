/**
 * Publish this event for custom analytics
 * publish(GA_EVENT, ["Category", "Action", "Label"]);
 */
subscribe(GA_EVENT, function(e, event) {
    while (event.length < 3) {
        event.push('');
    }
    ga('send', 'event', event[0], event[1], event[2]);
});

/**
 * Default data-attribute based event system
 * <a href="#" data-ga-event="Category, Action, Label"></a>
 */
subscribe(INIT, function() {
    var splitStr = function(str) {
        return str.split(',').map(function(item) {
            return item.trim();
        });
    };
    $('[data-ga-event]').click(function(e) {
        var $this = $(this);
        var event = splitStr($this.data(GA_EVENT));
        publish(GA_EVENT, event);
    });
});
