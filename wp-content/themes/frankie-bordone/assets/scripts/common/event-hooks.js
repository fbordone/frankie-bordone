/**
 * Event hooks for initializing components and handling layout changes. Initialize most components with:
 * subscribe(INIT, myInitFunction);
 * eg:
 * myInitFunction($outerContainer){
 *      $outerContainer.find('.component-wrapper');
 * }
 *
 * Hook layout end events (after a resize, etc) like this:
 * subscribe(LAYOUTEND, myUpdateFunction);
 *
 * For throttled layout updates instead of debounced, use:
 * subscribe(LAYOUT, myUpdateFunction);
 */
(function() {
    var debounce = 300;
    var throttle = 100;
    var docWidth = 0;
    var broadcastLayoutUpdate = _.throttle(function() {
        publish(LAYOUT);
    }, throttle);
    var broadcastLayoutEnd = _.debounce(function() {
        publish(LAYOUTEND);
    }, debounce);
    var layoutUpdate = function() {
        broadcastLayoutUpdate();
        broadcastLayoutEnd();
    };
    $(document).ready(function() {
        publish(INIT, $('body'));
        layoutUpdate();
        docWidth = mediaQuery.viewport.width();
    });
    $(window).on('load orientationchange', layoutUpdate);
    $(window).on('orientationchange', function() {
        setTimeout(layoutUpdate, 600);
    });

    if (isiOS) {
        $(window).on('resize', function() {
            var width = mediaQuery.viewport.width();
            if (width !== docWidth) {
                layoutUpdate();
            }
            docWidth = width;
        });
    } else {
        $(window).on('resize', layoutUpdate);
    }
    $(document).on('keyup', function(e) {
        if (e.keyCode === 27) {
            publish('key.esc');
        }
    });
})();
