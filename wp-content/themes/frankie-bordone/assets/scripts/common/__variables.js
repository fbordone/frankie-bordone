/**
 * Global variables within theme scope
 */
var ajaxUrl = window.sit.ajax_url;
var isiOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var publish = PubSub.publish;
var subscribe = PubSub.subscribe;
var unsubscribe = PubSub.unsubscribe;
var INIT = 'init';
var LAYOUT = 'layout';
var LAYOUTEND = 'layoutEnd';
var GA_EVENT = 'gaEvent';
