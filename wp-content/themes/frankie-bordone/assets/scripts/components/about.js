subscribe(INIT, function() {
    var $aboutHeader = $('.about__header');
    var $aboutDescription = $('.about__description');
    var $aboutSkills = $('.about__skills');

    var aboutToggle = function() {
        $(this).addClass('about__header--active').siblings().removeClass('about__header--active');

        if ($(this).is(':first-child')) {
            $aboutDescription.removeClass('display-none');
            $aboutSkills.addClass('display-none');
        } else {
            $aboutDescription.addClass('display-none');
            $aboutSkills.removeClass('display-none');
        }
    };

    $aboutHeader.click(aboutToggle);
});
