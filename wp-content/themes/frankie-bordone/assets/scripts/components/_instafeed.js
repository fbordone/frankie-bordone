subscribe(INIT, function() {
    var feed = new Instafeed({
        get: 'user',
        userId: '8433548453',
        accessToken: '8433548453.1677ed0.7e60135d132142d1b8f5e67fc6db7706',
        resolution: 'standard_resolution',
        target: 'instagram-feed',
        //jscs:disable maximumLineLength
        template: '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3"><a title="{{caption}}"><img src="{{image}}" /></a></div>',
        //jscs:enable maximumLineLength
        limit: 8
    });

    feed.run();
});
