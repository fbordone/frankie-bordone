subscribe(INIT, function() {
    var $contactForm = $('.contact-form');
    var $errors = $('.contact-form__messaging');
    var $success = $errors.find('.contact-form__success');
    var $danger = $errors.find('.contact-form__danger');
    var $submit = $contactForm.find('.contact-form__submit');

    $contactForm.on('submit', function(event) {
        event.preventDefault();
        $.get('/api/contact/get?' + $contactForm.serialize(), function(response) {
            $success.show();
            $danger.hide();
            $submit.attr('disabled', true);
        }).fail(function() {
            $success.hide();
            $danger.show();
            $submit.attr('disabled', false);
        });
    });
});
