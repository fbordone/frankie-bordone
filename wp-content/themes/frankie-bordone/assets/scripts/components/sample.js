(function() {
    var init = function() {
        console.log('sample component initialization. Please delete me.');
    };
    var layout = function() {
        console.log('sample component is updating layout. Please remove!');
    };
    PubSub.subscribe('init', init);
    PubSub.subscribe('layoutEnd', layout);
})();

