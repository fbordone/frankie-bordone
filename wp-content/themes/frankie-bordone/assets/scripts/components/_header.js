subscribe(INIT, function() {
    var $navbar = $('.header-nav');

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $navbar.addClass('header-nav__visible');
        } else {
            $navbar.removeClass('header-nav__visible');
        }
    });
});
