<?php

define('FB_CONFIG_DIR', __DIR__ . '/config');
define('FB_TEMPLATE_URL', get_template_directory_uri());

function fb_template_path()
{
    include \Underground\Util\TemplateWrapper::$main_template;
}

function fb_asset_path($filename)
{
    $dist_path = get_template_directory_uri() . '/dist/';
    $directory = dirname($filename) . '/';
    $file = basename($filename);
    static $manifest;

    if (empty($manifest)) {
        $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
        $manifest = new \Underground\Util\JsonManifest($manifest_path);
    }

    if (array_key_exists($file, $manifest->get())) {
        return $dist_path . $directory . $manifest->get()[$file];
    } else {
        return $dist_path . $directory . $file;
    }
}

function fb_template_part($path)
{
    $templates = [];
    $object = get_queried_object();
    if (is_page()) {
        if (!empty($object->post_name)) {
            $templates[] = "$path-{$object->post_name}.php";
        }
        $template = get_page_template_slug();
        if ($template && 0 === validate_file($template)) {
            $slug = pathinfo($template, PATHINFO_FILENAME);
            $templates[] = "$path-$slug.php";
        }
    } elseif (is_tax()) {
        if (!empty($object->slug)) {
            $templates[] = "$path-{$object->slug}.php";
            $templates[] = "$path-{$object->taxonomy}.php";
        }
    } elseif (is_singular()) {
        if (!empty($object->post_type)) {
            $templates[] = "$path-{$object->post_type}.php";
        }
    } elseif (is_post_type_archive()) {
        $post_type = get_query_var('post_type');
        if (is_array($post_type)) {
            $post_type = reset($post_type);
        }
        $templates[] = "$path-$post_type.php";
    }
    $templates[] = "$path.php";

    locate_template($templates, true, false);
}

function fb_title()
{
    if (is_home()) {
        if (get_option('page_for_posts', true)) {
            return get_the_title(get_option('page_for_posts', true));
        } else {
            return __('Latest Posts', 'frankiebordone');
        }
    } elseif (is_archive()) {
        return get_the_archive_title();
    } elseif (is_search()) {
        return sprintf(__('Search Results for %s', 'frankiebordone'), get_search_query());
    } elseif (is_404()) {
        return __('Not Found', 'frankiebordone');
    } else {
        return get_the_title();
    }
}

$GLOBALS['Theme'] = new \Theme\Theme();
