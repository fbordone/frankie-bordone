<?php
while (have_posts()) {
    the_post();
    get_template_part('templates/components/page-header');
    get_template_part('templates/modules/content-single', get_post_type());
}
